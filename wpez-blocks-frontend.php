<?php
/*
Plugin Name: WPezBlocks: Frontend
Plugin URI: https://gitlab.com/wpezsuite/WPezBlocks/wpez-blocks-frontend
Description: Uses the WPezBlocks' render_callback filters to createand render the blocks on the frontend.
Version: 0.0.0.1
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: http://AlchemyUnited.com
License: GPLv2+
Text Domain: wpez-blocks-fe
*/

namespace WPezBlocksFrontend;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezBlocksFrontend\App\ClassPlugin;

$str_php_ver_comp = '5.6.0';

// we reserve the right to use traits :)
if (version_compare(PHP_VERSION, $str_php_ver_comp, '<')) {
    exit(sprintf('This plugin - namespace: ' . __NAMESPACE__ . ' - requires PHP ' . esc_html($str_php_ver_comp) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION));
}


function autoloader( $bool = true ){

    if ( $bool !== true ) {
        return;
    }

    require_once 'App/Core/Autoload/ClassWPezAutoload.php';

    $new_autoload = new ClassWPezAutoload();
    $new_autoload->setPathParent( dirname( __FILE__ ) );
    $new_autoload->setNeedle(__NAMESPACE__ );
    $new_autoload->setReplaceSearch(__NAMESPACE__ . DIRECTORY_SEPARATOR);

    spl_autoload_register( [$new_autoload, 'WPezAutoload'], true );
}
autoloader();

function plugin($bool = true){

    if ( $bool === false ) {
        return;
    }

    $arr_args = [
        'plugin_dir' => __DIR__,
        'plugin_url' => plugins_url( null, __FILE__ ),
        'blocks_namespace' => 'wpez-blocks',
        'blocks_namespace_safe' => 'wpez_blocks'
    ];

    $new_plugin = new ClassPlugin($arr_args);
}

add_action('init', __NAMESPACE__ . '\plugin');
