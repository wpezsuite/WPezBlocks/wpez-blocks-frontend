<?php

namespace WPezBlocksFrontend\App\Plugin\Enqueue;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezBlocksExp\App\Core\ScriptsRegister\ClassScriptsRegister as ScriptsReg;
use WPezBlocksExp\App\Core\ScriptsRegister\ClassHooks as ScriptsHooks;
use WPezBlocksExp\App\Core\StylesRegister\ClassStylesRegister as StylesReg;
use WPezBlocksExp\App\Core\StylesRegister\ClassHooks as StylesHooks;


class ClassEnqueue {

    protected $_str_plugin_dir;
    protected $_str_plugin_url;
    protected $_str_path_slug;
    protected $_arr_scripts;
    protected $_arr_styles;


    public function __construct($arr_args = false) {

        if ( is_array( $arr_args )){

            $this->setPropertyDefaults( $arr_args );

            $this->scripts();
            $this->styles();
        }
    }

    protected function setPropertyDefaults($arr_args){

        $this->_str_plugin_dir = $arr_args['plugin_dir'];
        $this->_str_plugin_url = $arr_args['plugin_url'];
        $this->_str_path_slug = '/App/assets/dist';

        $this->_str_script_handle_primaray = 'wpezblocks-js';

        $this->_arr_scripts = [];
        $this->_arr_styles = [];


    }

    protected function scripts( $bool = true){

        if ( $bool === false ){
            return;
        }

        $this->_arr_scripts[] = [
            'handle' => 'wpezblocks-aos-js',
            'src' =>  "https://unpkg.com/aos@next/dist/aos.js",
            'deps' => [],
            'ver' => '3.0.0-beta',
            'hooks' => ['front'],
            'footer' => false
        ];

        $block_path = '/js/frontend.js';

        $this->_arr_scripts[] = [
            'handle' => 'wpez-blocks-frontend-js',
            'src' =>  $this->_str_plugin_url . $this->_str_path_slug . $block_path,
            'deps' => ['wpezblocks-aos-js'],
            'ver' => filemtime( $this->_str_plugin_dir .  $this->_str_path_slug . $block_path ),
            'hooks' => ['block_front']
        ];

        $new_rs = new ScriptsReg();

        $new_rs->loadScripts($this->_arr_scripts);

        $new_sh = new ScriptsHooks($new_rs);
        $new_sh->register();

    }

    protected function styles( $bool = true){

        if ( $bool === false ){
            return;
        }

        // https://unpkg.com/aos@next/dist/aos.css
        $this->_arr_styles[] = [
            'active' => false,  // <<<<
            'handle' => 'wpezblocks-aos-css',
            'src' =>  'https://unpkg.com/aos@2.3.1/dist/aos.css',
            'deps' => [],
            'ver' => '2.3.1',
            'hooks' => ['front']
        ];

        // https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css
        $this->_arr_styles[] = [
            'handle' => 'wpezblocks-animate-css',
            'src' =>  'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css',
            'deps' => [],
            'ver' => '3.7.2',
            'hooks' => ['front']
        ];

        $new_rs = new StylesReg();
        $new_rs->loadStyles($this->_arr_styles);

        $new_sh = new StylesHooks($new_rs);
        $new_sh->register();

    }


}

