<?php

namespace WPezBlocksFrontend\App\Blocks;


use WPezBlocksFrontend\App\Blocks\Views\ClassWrapperBlockBoxContent as WrapperBBC;
use WPezBlocksFrontend\App\Blocks\Views\ClassRichTextSingle as RTSingle;


class ClassTextHeading implements InterfaceGetBlock{

    public function __construct($arr_args)
    {

    }


    public function getBlock($str_ret = false, $arr_args = false)
    {

        if  ( is_string($str_ret) ){
            return $str_ret;
        }

        if ( ! is_array( $arr_args) ){
            return '';
        }
        // done just to be safer
        $arr_block_defs = [
            'block_name' => false,
            'attributes' => false,
            'context' => false,
            'attrs' => false,
            'attrs_oth' => false,
            'ani_block' => false,
            'ani_box' => false,
            'ani_content' => false
        ];
        $arr_args = array_merge($arr_block_defs, $arr_args);

        if ( ! is_array($arr_args['attrs']) || ! is_array($arr_args['attrs_oth'])){
            return '';
        }

        $arr_attrs_defs = [
            'textTagName' => false,
            'featureSet' => '01',
        ];
        $arr_attrs = array_merge($arr_attrs_defs, $arr_args['attrs']);

        $arr_attrs_oth_defs = [
            'blockAnchor' => '',
            'contentRichText' => ''
        ];
        $arr_attrs_oth = array_merge($arr_attrs_oth_defs, $arr_args['attrs_oth']);

        $arr_text_heading = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];

        if ( ! is_string($arr_attrs['textTagName'])
            || ! in_array( $arr_attrs['textTagName'], $arr_text_heading)
            || ! is_string($arr_attrs_oth['contentRichText']) ){
            return '';
        }

        // let's add some pairs to the attributes for making the wrapper's classes
        // $arr_attrs['blockAnchor'] = 'false';
        if ( isset($arr_attrs_oth['blockAnchor']) && ! empty($arr_attrs_oth['blockAnchor'])){
            $arr_attrs['blockAnchor'] = 'true';
        } else {
            unset( $arr_attrs['blockAnchor']);
        }

        if ( isset($arr_attrs['textDropCap']) && ! empty($arr_attrs['textDropCap'])){
            $arr_attrs['textDropCap'] = 'true';
        } else {
            unset( $arr_attrs['textDropCap']);
        }

        $new_rts = new RTSingle();
        $new_rts->setTag($arr_attrs['textTagName']);
        $new_rts->setText($arr_attrs_oth['contentRichText']);

        // featureSet = basic
        if ( $arr_attrs['featureSet'] < '25'){

            // for CSS consistency we'll use only colorCombo
        //    $arr_attrs['colorCombo'] = $arr_attrs['colorComboBasic'];
         //   unset($arr_attrs['colorComboBasic']);

            // let's only include the attributes Basic needs
            $arr_attrs_include = [
                'blockName',
                'featureSet',
                'blockWidth',
                'blockAnchor',
                'textTagName',
                'textAlign',
                'textSize',
                'textDropCap',
                'colorComboBasic',
              //  'colorCombo'
            ];

            $new_rts->setAttrs($arr_attrs);
            $new_rts->setAttrsInclude($arr_attrs_include);
            $new_rts->setAttrsOth($arr_attrs_oth);

            if ( ! $new_rts->getStatus() ){
                return '';
            }

            return $new_rts->getView();
        }

    // else ---------- NOT basic

      //  apply_filter('wpez_blocks_text_heading_inner','todo' );

        $new_rts->setAttrs([
            'isHeading' => 'true',
        ]);

        if ( ! $new_rts->getStatus() ){
            return '';
        }

        //$arr_attrs['block'] = 'wrapper';

        // it's not featureSet 01 so we don't want colorComboBasic
        unset($arr_attrs['colorComboBasic']);
        $new_wrap = new WrapperBBC();

        $new_wrap->setInner($new_rts);
        $new_wrap->setAttrs($arr_attrs);
        $new_wrap->setAttrsExclude([
            'colorComboBasic'
        ]);
        $new_wrap->setAttrsOth($arr_attrs_oth);
        //$new_wrap->setAttrsExclude();

        // We only set the ani if necessary
        if ( $arr_attrs['featureSet'] === '99') {
            $new_wrap->setAniFlag(true);
            $new_wrap->setAniBlock($arr_args['ani_block']);
            $new_wrap->setAniBox($arr_args['ani_box']);
            $new_wrap->setAniContent($arr_args['ani_content']);
        }

        if ( ! $new_wrap->getStatus() ){
            return '';
        }

        return $new_wrap->getView();

    }

}

