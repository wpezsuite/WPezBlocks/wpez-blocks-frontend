<?php

namespace WPezBlocksFrontend\App\Blocks\Views;

interface InterfaceGetView
{

    public function getStatus();

    public function getView($arr_args);

}
