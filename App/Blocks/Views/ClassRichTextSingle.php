<?php

namespace WPezBlocksFrontend\App\Blocks\Views;


class ClassRichTextSingle extends AbstractClassBaseView{

    protected $_arr_attrs_include;
    protected $_arr_attrs_exclude;
    protected $_arr_attrs;
    protected $_arr_attrs_oth;

    protected $_str_tag;
    protected $_str_text;

    use \WPezBlocksFrontend\App\Core\Setters\TraitSetString;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetArray;

    public function __construct( $arr_args = [])
    {
        $this->setPropertyDefaults();
    }


    protected function  setPropertyDefaults(){

        $this->_arr_attrs_include = [];
        $this->_arr_attrs_exclude = [];
        $this->_arr_attrs = [];
        $this->_arr_attrs_oth = [];

        $this->_str_tag = false;
        $this->_str_text = false;

    }

    public function setAttrs($arr = false)
    {

        return $this->setArray('_arr_attrs', $arr);

    }

    public function setAttrsOth($arr = false)
    {

        return $this->setArray('_arr_attrs_oth', $arr);

    }

    public function setAttrsInclude($arr = false)
    {

        return $this->setArray('_arr_attrs_include', $arr);

    }

    /*
     * when we're making the class list for the outer wrapper which attrs should be ignored (e.g., blockAnchor)
     */
    public function setAttrsExclude($arr = false)
    {

        return $this->setArray('_arr_attrs_exclude', $arr);

    }

    public function setTag( $str = false ){

        return $this->setString('_str_tag', $str);
    }

    public function setText( $str = false ){

        return $this->setString('_str_text', $str);
    }

    public function getStatus(){

        if ( ! is_string($this->_str_tag) || ! is_string($this->_str_text) ){
            return false;
        }
        return true;

    }

    public function getView($arr_args = [])
    {
        $str_ret = '';
        if ( ! $this->getStatus() ){
            return $str_ret;
        }

        $str_ret .= '<' . esc_attr($this->_str_tag);
        if (isset($this->_arr_attrs_oth['blockAnchor']) && !empty(esc_attr($this->_arr_attrs_oth['blockAnchor']))) {
            $str_ret .= ' id="' . esc_attr($this->_arr_attrs_oth['blockAnchor']) . '"';
        }

        $str_ret .= ' class="';
        if (isset($this->_arr_attrs_oth['className']) && !empty(esc_attr($this->_arr_attrs_oth['className']))) {
            $str_ret .=  esc_attr($this->_arr_attrs_oth['className']) . ' ';
        }
        $str_ret .= $this->className($this->_arr_attrs, $this->_arr_attrs_include, $this->_arr_attrs_exclude);
        $str_ret .= '">';

        $str_ret .= wp_kses_post($this->_str_text);
        $str_ret .= '</' . esc_attr($this->_str_tag) . '>';

        return $str_ret;
    }

}