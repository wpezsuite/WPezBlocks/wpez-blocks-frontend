<?php

namespace WPezBlocksFrontend\App\Blocks\Views;

// class ClassWrapperBlockBoxContent implements InterfaceGetView
class ClassWrapperBlockBoxContent extends AbstractClassBaseView
{

    protected $_obj_content_inner;
    protected $_arr_attrs;
    protected $_arr_attrs_oth;
    protected $_arr_attrs_include;
    protected $_arr_attrs_exclude;
    protected $_bool_ani;
    protected $_arr_ani_block;
    protected $_arr_ani_box;
    protected $_arr_ani_content;

    use \WPezBlocksFrontend\App\Core\Setters\TraitSetArray;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetBool;

    public function __construct()
    {
        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults()
    {

        $this->_obj_content_inner = false;
        $this->_arr_attrs = false;
        $this->_arr_attrs_oth = false;
        $this->_arr_attrs_include = [];
        $this->_arr_attrs_exclude = [];
        $this->_bool_ani = false;
        $this->_arr_ani_block = [];
        $this->_arr_ani_box = [];
        $this->_arr_ani_content = [];

    }

    public function setInner($obj)
    {

        if ($obj instanceof InterfaceGetView) {
            $this->_obj_content_inner = $obj;
            return true;
        }
        return false;
    }

    public function setAttrs($arr = false)
    {

        return $this->setArray('_arr_attrs', $arr);

    }

    public function setAttrsOth($arr = false)
    {

        return $this->setArray('_arr_attrs_oth', $arr);

    }

    public function setAttrsInclude($arr = false)
    {

        return $this->setArray('_arr_attrs_include', $arr);

    }

    /*
     * when we're making the class list for the outer wrapper which attrs should be ignored (e.g., blockAnchor)
     */
    public function setAttrsExclude($arr = false)
    {

        return $this->setArray('_arr_attrs_exclude', $arr);

    }

    // -------------------------------------------------------

    public function setAniFlag($bool = false)
    {

        return $this->setBool('_bool_ani', $bool);

    }

    public function setAniBlock($arr = false)
    {

        return $this->setArray('_arr_ani_block', $arr);

    }

    public function setAniBox($arr = false)
    {

        return $this->setArray('_arr_ani_box', $arr);

    }

    public function setAniContent($arr = false)
    {

        return $this->setArray('_arr_ani_content', $arr);

    }

    public function getStatus()
    {

        if ($this->_obj_content_inner !== false && $this->_arr_attrs !== false && $this->_arr_attrs_oth !== false) {
            return true;
        }
        return false;
    }


    public function getView($arr_args = [])
    {

        $str_ani_block_class = '';
        $arr_ani_box['class'] = [];
        $str_ani_box_class = '';
        $arr_ani_content['class'] = [];
        $str_ani_content_class = '';

        if ($this->_bool_ani !== false) {

            $str_slug = 'wpez-blocks-ani';

            $arr_ani_block = $this->parseAni('block', $this->_arr_ani_block);
            $str_ani_block_flag = $this->aniClassFlag($arr_ani_block);
            $str_ani_block_class = $str_slug . '-' . $str_ani_block_flag;

            $arr_ani_box = $this->parseAni('box', $this->_arr_ani_box);
            $str_ani_box_flag = $this->aniClassFlag($arr_ani_box);
            $str_ani_box_class = $str_slug . '-' . $str_ani_box_flag;

            $arr_ani_content = $this->parseAni('content', $this->_arr_ani_content);
            $str_ani_content_flag = $this->aniClassFlag($arr_ani_content);
            $str_ani_content_class = $str_slug . '-' . $str_ani_content_flag;

            $this->_arr_attrs['aniBlock'] = $str_ani_block_flag;
            $this->_arr_attrs['aniBox'] = $str_ani_box_flag;
            $this->_arr_attrs['aniContent'] = $str_ani_content_flag;
        }

        // let's add some pairs to the attributes for making the wrapper's classes
        //   $this->_arr_attrs['block'] = 'wrapper';
        //  $this->_arr_attrs['blockAnchor'] = 'false';

        //   if ( isset($this->_arr_attrs_oth['blockAnchor']) && ! empty($this->_arr_attrs_oth['blockAnchor'])){
        //       $this->_arr_attrs['blockAnchor'] = 'true';
        //    }

        // TODO X? $this->_arr_attrs['blockAnchor'] = isset($this->_arr_attrs_oth['blockAnchor']) ? : false;

        $str_ret = '';
        $str_ret .= '<div';
        if (isset($this->_arr_attrs_oth['blockAnchor']) && !empty(esc_attr($this->_arr_attrs_oth['blockAnchor']))) {
            $str_ret .= ' id="' . esc_attr($this->_arr_attrs_oth['blockAnchor']) . '"';
        }

        $str_ret .= ' class="wpez-blocks-block-wrapper ';
        if (isset($this->_arr_attrs_oth['className']) && !empty(esc_attr($this->_arr_attrs_oth['className']))) {
            $str_ret .= esc_attr($this->_arr_attrs_oth['className']);
        }
        $str_ret .= ' ' . $this->className($this->_arr_attrs, $this->_arr_attrs_include, $this->_arr_attrs_exclude) . '">';

        // block
        $str_ret .= '<div class="wpez-blocks-sib wpez-blocks-block';
        if ($this->_bool_ani !== false) {
            $str_ret .=  ' ' . esc_attr($str_ani_block_class) . ' ' . implode(' ', $arr_ani_block['class']) . '" ' . implode(' ', $arr_ani_block['data']);
        } else {
            $str_ret .= '"';
        }
        $str_ret .= '>';
        $str_ret .= '</div>';

        // box
        $str_ret .= '<div class="wpez-blocks-sib wpez-blocks-box';
        if ($this->_bool_ani !== false) {
            $str_ret .=  ' ' . esc_attr($str_ani_box_class) . ' ' . implode(' ', $arr_ani_box['class']) . '" ' . implode(' ', $arr_ani_box['data']);
        } else {
            $str_ret .= '"';
        }
        $str_ret .= '>';
        $str_ret .= '</div>';

        // content
        $str_ret .= '<div class="wpez-blocks-sib wpez-blocks-content';
        if ($this->_bool_ani !== false) {
            $str_ret .=  ' ' . esc_attr($str_ani_content_class) . ' ' . implode(' ', $arr_ani_content['class']) . '" ' . implode(' ', $arr_ani_content['data']);
        } else {
            $str_ret .= '"';
        }
        $str_ret .= '>';
        $str_ret .= '<span class="wpez-blocks-offset-height">';

        // content - inner
        $str_ret .= $this->_obj_content_inner->getView($arr_args);

        $str_ret .= '</span>';
        $str_ret .= '</div>';
        $str_ret .= '</div>';

        return $str_ret;

    }

    protected function aniClassFlag($arr = [])
    {
        if (isset($arr['data'])
            && is_array($arr['data'])
            && !empty($arr['data'])) {
            return 'true';
        }
        return 'false';
    }

    protected function parseAni($str_type = false, $arr = false)
    {

        $str_type = trim($str_type);
        $arr_types = ['block', 'box', 'content'];

        $arr_ret = [];
        $arr_ret['data'] = [];
        $arr_ret['class'] = [];
        if (!in_array($str_type, $arr_types)) {
            return $arr_ret;
        }

        /*
        $this->_arr_attrs = [
            'AniName',
            'Duration',
            'Delay',
            'Iterations'
        ];
        */

        /*
        $arr_duration = [
            'shortest' => '500',
            'shorter' => '800',
            'short' => '1200',
            'long' => '2000',
            'longer' => '3000',
            'longest' => '4000'
        ];
        */

        // map to animate.css
        // https://github.com/daneden/animate.css
        $arr_duration = [
            'none' => 'duration-none',
            'shortest' => 'faster',
            'shorter' => 'fast',
            'short' => 'default',
            'long' => 'slow',
            'longer' => 'slower',
            'longest' => 'slowest'
        ];

        /*
        $arr_delay = [
            'none' => '0',
            'shortest' => '500',
            'shorter' => '1000',
            'short' => '1500',
            'long' => '2000',
            'longer' => '2500'
        ];
        */

        // map to animate.css
        // https://github.com/daneden/animate.css
        $arr_delay = [
            'none' => 'delay-0s',
            'shortest' => 'delay-1s',
            'shorter' => 'delay-2s',
            'short' => 'delay-3s',
            'long' => 'delay-4s',
            'longer' => 'delay-5s'
        ];

        $arr_offset = [
            'minus_most' => '-250',
            'minus_more' => '-150',
            'minus_some' => '-75',
            'none' => '0',
            'plus_some' => '75',
            'plus_more' => '150',
            'plus_most' => '250'
        ];

        $arr_def = [
            $str_type . 'AniName' => 'none',
            $str_type . 'AniDuration' => 'none',
            $str_type . 'AniDelay' => 'none',
            $str_type . 'AniIterations' => '1',
            $str_type . 'AniEasing' => 'ease',
            $str_type . 'AniEleAnchor' => 'center',
            $str_type . 'AniWinAnchor' => 'bottom',
            $str_type . 'AniOffset' => 'none',
        ];


        $arr_merge = array_merge($arr_def, $arr);


        if ($arr_merge[$str_type . 'AniName'] === 'none' || $arr_merge[$str_type . 'AniDuration'] === 'none') {
            return $arr_ret;
        }

        $arr_ret['data'][] = 'data-aos="' . esc_attr($arr_merge[$str_type . 'AniName']) . '"';

        $temp = 'none';
        if (isset($arr_offset[$arr_merge[$str_type . 'AniOffset']])) {
            $temp = $arr_offset[$arr_merge[$str_type . 'AniOffset']];
        }
        $arr_ret['data'][] = 'data-aos-offset="' . esc_attr($temp) . '"';

        $arr_ret['data'][] = 'data-aos-anchor-placement="' . esc_attr($arr_merge[$str_type . 'AniEleAnchor']) . '-' . esc_attr($arr_merge[$str_type . 'AniWinAnchor']) . '"';

        /*
         $arr_ret[] = 'data-aos-easing="' . esc_attr($arr_merge[$str_type . 'AniEasing']) . '"';
        */


        // -----------

        $temp = '1';
        if (isset($arr_merge[$str_type . 'AniIterations'])) {
            $temp = $arr_merge[$str_type . 'AniIterations'];
        }
        if ($temp === 'loop') {
            $arr_ret['class'][] = 'infinite';
        } else {
            $arr_ret['class'][] = 'animation-iteration-count-' . esc_attr($temp);
        }

        $temp = '0';
        if (isset($arr_delay[$arr_merge[$str_type . 'AniDelay']])) {
            $temp = $arr_delay[$arr_merge[$str_type . 'AniDelay']];
        }
        $arr_ret['class'][] = esc_attr($temp);

        $temp = 'default';
        if (isset($arr_duration[$arr_merge[$str_type . 'AniDuration']])) {
            $temp = $arr_duration[$arr_merge[$str_type . 'AniDuration']];
        }
        $arr_ret['class'][] = esc_attr($temp);

        // TODO - iterations ['class']

        return $arr_ret;
    }

}
