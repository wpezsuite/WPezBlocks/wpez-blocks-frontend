<?php

namespace WPezBlocksFrontend\App\Blocks\Views;


abstract class AbstractClassBaseView implements InterfaceGetView{


    protected function className($arr = false, $arr_inc = [], $arr_exc = [])
    {

        if (!is_array($arr) || !is_array($arr_inc) || !is_array($arr_exc)) {
            return '';
        }
        $arr_ret = [];
        $bool_include = ! empty($this->_arr_attrs_include);
        foreach ($arr as $key => $val) {

            if ( empty($val) || ! is_string($val) ){
                continue;
            }

            if ($bool_include && ! in_array($key, $arr_inc)) {
                continue;
            } elseif (in_array($key, $arr_exc)) {
                continue;
            }

            $arr_ret[] = 'wpez-blocks-' . esc_attr($key) . '-' . esc_attr($val);

        }

        return implode(' ', $arr_ret);
    }
}
