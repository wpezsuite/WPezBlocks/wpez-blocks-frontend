window.addEventListener("load", function (event) {

    var forEach = function (array, callback, scope) {
        for (var i = 0; i < array.length; i++) {
            callback.call(scope, i, array[i]);
        }
    };

    var blocks = document.getElementsByClassName('wpez-blocks-block-wrapper');

    if (blocks[0]) {

        forEach(blocks, function (index, block) {

            let sibs = block.getElementsByClassName('wpez-blocks-sib');
            let contentDiv = block.getElementsByClassName('wpez-blocks-content')[0];
            let content = contentDiv.getElementsByClassName('wpez-blocks-offset-height')[0];
       //     let content = contentDiv;

            if (typeof content !== 'undefined') {
                // contentDiv.style.position = 'relative';
                let contentHeight = content.offsetHeight;
                // console.log(contentHeight);
                block.style.minHeight = `${contentHeight}px`;
                forEach(sibs, function (index, sib) {
                    sib.style.minHeight = `${contentHeight}px`;
                });
                // contentDiv.style.removeProperty('position');

            }

        });
    }


    let aosConfig = {};
    if (typeof wpezBlocksAOS !== 'undefined' && typeof wpezBlocksAOS === 'object') {
        aosConfig = wpezBlocksAOS;
    } else {
        console.log('No config: aosConfig');
    }
    /*
    useClassNames: true,
    initClassName: false,
    animatedClassName: 'animated',
      offset: 120,
      delay: 0,
      easing: 'ease',
      duration: 400,
      disable: false,
      once: false,
      startEvent: 'DOMContentLoaded',
      throttleDelay: 99,
      debounceDelay: 50,
      disableMutationObserver: false,
     */
    let aosActive = (aosConfig.hasOwnProperty('active')) ? aosConfig.active : true;

    let useClassNames = aosConfig.useClassNames || true;
    let initClassName = aosConfig.initClassName || false;
    let animatedClassName = aosConfig.animatedClassName || 'animated';

    let aosOffset = aosConfig.offset || 120;
    let aosDelay = aosConfig.delay || 0;
    let aosEasing = aosConfig.easing || 'ease';
    let aosDuration = aosConfig.duration || 400;
    let aosDisable = (aosConfig.hasOwnProperty('disable')) ? aosConfig.disable : false;

    let aosOnce = (aosConfig.hasOwnProperty('once')) ? aosConfig.once : false;
    // let aosMirror = aosConfig.mirror || false;
    // let aosAnchorPlacement = aosConfig.anchorPlacement || 'top-bottom';
    let aosStartEvent = aosConfig.startEvent || 'DOMContentLoaded';
    // let aosAnimatedClassName = aosConfig.animatedClassName || 'aos-animate';

    // let aosInitClassName = aosConfig.initClassName || 'aos-init';
    // let aosUseClassNames = aosConfig.useClassNames || false;

    let aosThrottleDelay = aosConfig.throttleDelay || 99;
    let aosDebounceDelay = aosConfig.debounceDelay || 50;
    let aosDisableMutationObserver = (aosConfig.hasOwnProperty('disableMutationObserver')) ? aosConfig.disableMutationObserver : false;


    let aosAnchor = aosConfig.anchor || null;

    if (aosActive) {
        AOS.init({

            useClassNames: useClassNames,
            initClassName: initClassName,
            animatedClassName: animatedClassName,

            offset: aosOffset,
            delay: aosDelay,
            easing: aosEasing,
            duration: aosDuration,
            disable: window.innerWidth < aosDisable,

            once: aosOnce,
            // mirror: aosMirror,
            // anchorPlacement: aosAnchorPlacement,
            startEvent: aosStartEvent,
            // animatedClassName: aosAnimatedClassName,

            // initClassName: aosInitClassName,
            // useClassNames: aosUseClassNames,
            throttleDelay: aosThrottleDelay,
            debounceDelay: aosDebounceDelay,
            disableMutationObserver: aosDisableMutationObserver,

            anchor: aosAnchor

        });
    } else {
        console.log('AOS.init - not active');
    }
}, false);