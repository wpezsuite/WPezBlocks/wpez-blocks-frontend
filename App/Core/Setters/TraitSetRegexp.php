<?php

namespace WPezBlocksFrontend\App\Core\Setters;

trait TraitSetRegexp {

    protected function setRegexp( $str_prop = false, $mix = false, $arr_regexp = [] ) {

        // http://php.net/manual/en/filter.filters.validate.php
        // https://www.w3schools.com/php/filter_validate_regexp.asp
        if ( property_exists( $this, $str_prop )
             && $mix !== false
             && filter_var( $mix, FILTER_VALIDATE_REGEXP, $arr_regexp ) ) {

            $this->$str_prop = $mix;

            return true;
        }

        return false;
    }
}