<?php

namespace WPezBlocksFrontend\App\Core\Setters;

trait TraitSetInteger {

    protected function setInteger( $str_prop = false, $int = false, $arr_flags = [] ) {

        // http://php.net/manual/en/filter.filters.validate.php
        if ( property_exists( $this, $str_prop )
             && is_array( $arr_flags )
             && filter_var( $int, FILTER_VALIDATE_INT, $arr_flags ) ) {

            $this->$str_prop = $int;

            return true;
        }

        return false;
    }
}