<?php

namespace WPezBlocksFrontend\App\Core\Setters;

trait TraitSetHex {

	protected function setHex( $str_prop = false, $str_hex = false, $str_prefix = '#') {

		// http://php.net/manual/en/filter.filters.validate.php
		if ( property_exists( $this, $str_prop ) ) {

			$str_hex = ltrim( $str_hex, '#' );

			if ( preg_match( '/([A-Fa-f0-9]{3}){1,2}$/', $str_hex ) ) {
				// TODO - prefix with # is it doesn't exist
				$this->$str_prop = $str_prefix  . $str_hex;

				return true;
			}
		}

		return false;
	}
}




