<?php

namespace WPezBlocksFrontend\App\Core\Setters;

trait TraitSetIP {

    protected function setIP( $str_prop = false, $str_ip = false, $arr_flags = [] ) {

        // http://php.net/manual/en/filter.filters.validate.php
        if ( property_exists( $this, $str_prop )
             && is_array( $arr_flags )
             && filter_var( $str_ip, FILTER_VALIDATE_IP, $arr_flags ) ) {

            $this->$str_prop = $str_ip;

            return true;
        }

        return false;
    }
}