<?php

namespace WPezBlocksFrontend\App\Core\Setters;

trait TraitSetString {

    protected function setString( $str_prop = false, $str = false, $arr_len = [], $bool_ltrim = true ) {

        $arr_len_default = [
            'min_len' => 0,
            'max_len' => false
        ];
        if ( is_array( $arr_len ) ) {

            $arr_len_default = array_merge( $arr_len_default, $arr_len );
        }
        if ( empty ( ltrim( $str ) ) ) {

        }

        if ( property_exists( $this, $str_prop ) && is_string( $str ) ) {

            // without this, e.g., 3x spaces would have a strlen = 3
            if ( $bool_ltrim && empty ( ltrim( $str ) ) ) {
                $str = '';
            }

            if ( strlen( $str ) >= absint( $arr_len_default['min_len'] )
                 && ( $arr_len_default['max_len'] === false || strlen( $str ) <= absint( $arr_len_default['max_len'] ) ) ) {

                $this->$str_prop = $str;
                return true;
            }

            return false;
        }
    }
}