<?php

namespace WPezBlocksFrontend\App\Core\Setters;

trait TraitSetURL {

    protected function setURL( $str_prop = false, $str_url = false, $arr_flags = [] ) {

        if ( property_exists( $this, $str_prop )
             && is_string($str_url)
             && is_array( $arr_flags )
             && filter_var( $str_url, FILTER_VALIDATE_URL, $arr_flags ) ) {

            $this->$str_prop = $str_url;

            return true;
        }

        return false;
    }
}