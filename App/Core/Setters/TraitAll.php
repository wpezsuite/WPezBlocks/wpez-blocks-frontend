<?php

namespace WPezBlocksFrontend\App\Core\Setters;

trait TraitAll {

    use \WPezBlocksFrontend\App\Core\Setters\TraitSetArray;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetBool;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetEmail;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetFloat;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetHex;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetInteger;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetIP;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetRegexp;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetString;
    use \WPezBlocksFrontend\App\Core\Setters\TraitSetURL;

}