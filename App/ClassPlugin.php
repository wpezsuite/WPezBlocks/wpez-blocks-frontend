<?php

namespace WPezBlocksFrontend\App;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezBlocksFrontend\App\Plugin\Enqueue\ClassEnqueue as Enqueue;

use WPezBlocksFrontend\App\Blocks\ClassTextHeading as TextHeading;


class ClassPlugin {


    public function __construct($arr_args = false) {

        if ( is_array( $arr_args )){

            // $this->setPropertyDefaults($arr_args);

            $this->enqueue($arr_args);

            $new_text_heading = new TextHeading($arr_args);

            add_filter('wpez_blocks_text_heading', [$new_text_heading, 'getBlock'], 2, 20 );
        }
    }

    protected function setPropertyDefaults($arr_args = []){

    }

    protected function enqueue($arr_args){

        new Enqueue($arr_args);

    }

}

